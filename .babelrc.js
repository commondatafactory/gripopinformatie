module.exports = {
  "presets": ["next/babel"],
  "plugins": [
    "inline-react-svg",
    [
      "babel-plugin-styled-components",
      {
        "ssr": true,
        "minify": true,
        "transpileTemplateLiterals": true,
        "pure": true,
        "displayName": true,
        "preprocess": false
      }
    ]
  ]
}
