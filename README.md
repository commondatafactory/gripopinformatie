# Wie doet er mee 

Een simpele applicatie die het mogelijk maakt een kaart te maken waarop de aangegeven gemeente zijn ingekleurd. Upload een Excel bestand met daarin 1 kolom met gemeente namen. Deze gemeenten worden in gekleurd op de kaart. Pas de titels aan en exporteer het plaatje naar een PDF bestand. Zo kunt u bijvoorbeeld makkelijk een kaartje maken van welke gemeente mee doen in uw project! 

In opdracht van Grip Op Informatie & VNG realisatie. 
Onderdeel van de CommonDataFactory.

## Getting started

Use the package manager npm to install Grip Op Informatie.

```bash
npm install
```

## How to use

```bash
npm run
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

todo: example excel.

