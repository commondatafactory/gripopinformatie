image: node:19-alpine3.16

stages:
  - yarn-install
  - lint_typecheck_test
  - build_and_push
  - deploy

default:
  cache: &global_cache
    key: "${CI_PROJECT_PATH}"
    paths:
      - node_modules/
      - .next/cache/
    policy: pull-push

variables:
  WDM_SHA_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  WDM_REF_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  NPM_CACHE_DIR: ${CI_PROJECT_DIR}/.npm-cache

yarn-install:
  stage: yarn-install
  script:
    - yarn install --frozen-lockfile --prefer-offline --no-progress --color=false --quiet

Lint, typecheck & test:
  stage: lint_typecheck_test
  cache:
    <<: *global_cache
    policy: pull
  script:
    - yarn lint
    # - yarn type-check
    # - yarn test
  coverage: /All\sfiles.*?\s+(\d+.\d+)/
  artifacts:
    expire_in: 1 month
    paths:
      - coverage
  needs: ["yarn-install"]


.build:
  image: registry.gitlab.com/commondatafactory/dockerimages/yarnbuilder:4fbe11c20230316v2
  cache:
    <<: *global_cache
    policy: pull
  stage: build_and_push
  services:
    - docker:20.10.24-dind
  variables:
    DOCKER_TLS_CERTDIR: '/certs'
    NEXT_TELEMETRY_DISABLED: 1
  before_script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
  only:
    - branches@commondatafactory/wiedoetermee
  needs: ['Lint, typecheck & test']

Build and push WDM:
  extends: .build
  script:
    - |
      yarn build && \
      find out -name "*.js" | xargs -I % sh -c 'gzip %' && \
      docker build \
      --cache-from $CI_REGISTRY_IMAGE/wdm:latest \
        --tag $WDM_SHA_TAG --tag $WDM_REF_TAG . && \
      docker push $WDM_SHA_TAG && \
      docker push $WDM_REF_TAG
  except:
    - main

Build and push WDM latest:
  extends: .build
  script:
    - |
      yarn build && \
      find out -name "*.js" | xargs -I % sh -c 'gzip %' && \
      docker build \
        --cache-from $CI_REGISTRY_IMAGE/wdm:latest \
        --tag $CI_REGISTRY_IMAGE/wdm:latest \
        --tag $WDM_SHA_TAG --tag $WDM_REF_TAG . && \
      docker push $WDM_SHA_TAG && \
      docker push $WDM_REF_TAG && \
      docker push $CI_REGISTRY_IMAGE/wdm:latest
  only:
    - main

.deploy:
  extends: .build
  stage: deploy

Deploy Acceptation:
  extends: .deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  before_script:
    - git clone https://gitlab.com/commondatafactory/helm-charts/wiedoetermee.git helm/wiedoetermee
    - |
      cat >./helm/wiedoetermee/overrides.yaml <<EOL
      wdm:
        enabled: true
        image:
          tag: "${CI_COMMIT_SHORT_SHA}"
        replicaCount: 2
        ingress:
          enabled: true
          hosts:
            - acc.wdm.commondatafactory.nl
          tls:
          - hosts:
            - acc.wdm.commondatafactory.nl
            secretName: wdm-acc-commondatafactory-nl-ingress-tls
      EOL

      cat ./helm/wiedoetermee/overrides.yaml
  script:
    - |
      kubectl config use-context commondatafactory/ci/kubernetes-agents:cdf-datacluster
      helm upgrade --install wiedoetermee ./helm/wiedoetermee \
        --namespace cdf-acc \
        --values ./helm/wiedoetermee/values.yaml \
        --values ./helm/wiedoetermee/overrides.yaml
  environment:
    name: acc
  only:
    - develop
  needs: ['Build and push WDM']

Deploy Production:
  extends: .deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  variables:
    WDM_URL: wiedoetermee.commondatafactory.nl
  before_script:
    - git clone https://gitlab.com/commondatafactory/helm-charts/wiedoetermee.git helm/wiedoetermee
    - |
      cat >./helm/wiedoetermee/overrides.yaml <<EOL
      wdm:
        enabled: true
        image:
          tag: "${CI_COMMIT_SHORT_SHA}"
        replicaCount: 3
        ingress:
          enabled: true
          hosts:
            - wiedoetermee.commondatafactory.nl
          tls:
          - hosts:
            - wiedoetermee.commondatafactory.nl
            secretName: wdm-commondatafactory-nl-ingress-tls
      EOL

      cat ./helm/wiedoetermee/overrides.yaml
  script:
    - |
      kubectl config use-context commondatafactory/ci/kubernetes-agents:cdf-datacluster
      helm upgrade --install wiedoetermee ./helm/wiedoetermee \
        --namespace cdf-prod \
        --values ./helm/wiedoetermee/values.yaml \
        --values ./helm/wiedoetermee/overrides.yaml
  environment:
    name: prod
  only:
    - main
  needs: ['Build and push WDM latest']
