// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { GlobalStyles, defaultTheme } from '@commonground/design-system'
import Head from 'next/head'
import type { AppProps } from 'next/app'

import { ThemeProvider } from 'styled-components'
import '@fontsource/source-sans-pro/latin.css'
import '../src/index.css'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={defaultTheme}>
      <GlobalStyles />
      <Head>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      <Component {...pageProps} />
    </ThemeProvider>
  )
}

export default MyApp
