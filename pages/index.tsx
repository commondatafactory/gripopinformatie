// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'

import InputContainer from '../src/components/InputContainer/InputContainer'
import MapContainer from '../src/components/MapContainer/MapContainer'
import { AppContextProvider } from '../src/contextAPI'
import styled from 'styled-components'

const StyledApp = styled.div`
  display: flex;
  overflow: hidden;
  @media only screen and (max-width: 1080px) {
    & {
      flex-direction: column;
      justify-content: center;
      align-items: center;
      align-content: center;
    }
  }
`
function App() {
  return (
    <AppContextProvider>
      <StyledApp id="app">
        <InputContainer />
        <MapContainer />
      </StyledApp>
    </AppContextProvider>
  )
}

export default App
