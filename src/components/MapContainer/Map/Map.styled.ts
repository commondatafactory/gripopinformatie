// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'

export const Container = styled.div`
  overflow: hidden;
  grid-column: 1 / span 3;
  grid-row: 2 / span 2;
  justify-self: stretch;
  align-self: stretch;
  display: flex;
  justify-content: center;

  svg {
    height: 100%;
  }
`
