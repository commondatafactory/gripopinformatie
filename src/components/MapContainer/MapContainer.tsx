// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React, { useState } from 'react'
import styled from 'styled-components'
import { SelectComponent } from '@commonground/design-system'
import VNGLogo from '../../../public/vng-logo.svg'
import Map from './Map/Map'
import html2canvas from 'html2canvas'
declare global {
  interface Window {
    chrome: any
  }
}

const StyledMapContainer = styled.div`
  height: 100vh;
  width: 50%;
  background-color: #ffffff;
  padding: ${(div) => div.theme.tokens.spacing10};
  margin: 0;
  overflow: hidden;
  display: flex;
  flex-direction: column;

  @media only screen and (max-width: 1080px) {
    & {
      width: 100%;
    }
  }
`

const DownloadContainer = styled.div`
  height: 90%;
  display: grid;
  grid-template-rows: 10% 1fr 3fr;
  grid-template-columns: repeat(3, 1fr);
  border: #d2d2d2 1px solid;
  padding: 0.5rem;
`
const Legend = styled.div`
  grid-column: 1 / span 1;
  grid-row: 2 / span 1;
`

const LegendItem = styled.div`
  display: flex;
  flex-direction: row;

  > div {
    width: 40px;
    height: 20px;
    border-radius: 2px;
    margin: 0.2rem;
  }
`

const TextInput = styled.input`
  appearance: none;
  outline: none;
  border: none;
  width: 100%;
  margin-left: 1rem;
  background: transparent;
`

const TitleInput = styled(TextInput)`
  grid-column: 1 / span 3;
  grid-row: 1 / span 1;
  margin: 0;
  line-height: 125%;
  font-weight: 700;
  text-align: center;
  font-size: 1.6rem;
`

const Source = styled.div`
  grid-column: 3 / span 1;
  grid-row: 3 / span 1;
  align-self: end;
  justify-self: end;
  display: flex;
  align-items: center;

  p {
    color: grey;
    font-size: 0.8rem;
    margin: 0;
  }
`

const ExportContainer = styled.div`
  height: 10%;
  text-align: center;
  padding: ${(div) => div.theme.tokens.spacing05};
  font-weight: 400;
`

const StyledSelectComponent = styled(SelectComponent)`
  margin: 0 auto;
  text-align: left;
`

const StyledSelectSource = styled.select`
  border: none;
  background-color: transparent;
  font-size: 0.8rem;
  font-weight: 700;
  color: grey;
  margin-right: 15px;
`

const MapContainer = () => {
  const [mapTitle, setMapTitle] = useState('')
  const [downloadValue, setDownloadValue] = useState(null)
  const [legendText1, setLegendText1] = useState('')
  const [legendText2, setLegendText2] = useState('')
  const [year, setYear] = useState('2023')

  function openPrintWindow(type) {
    const newWindowWidth = type === 'pdf' ? screen.width : 1
    const newWindowHeight = type === 'pdf' ? screen.height : 1

    const printWindow = window.open(
      '',
      'Print kaart',
      `left=0,top=0,width=${newWindowWidth},height=${newWindowHeight},toolbar=0,scrollbars=0,status=0,addressbar=0`,
    )

    printWindow.document.write('<link rel="stylesheet" type="text/css" href="/print.css"/>')
    printWindow.document.write('<meta name="viewport" content="width=device-width, initial-scale=1" />')
    printWindow.document.write('<html><head><title>Wie doet er mee kaart</title>')
    printWindow.document.write('</head><body>')
    printWindow.document.write('<div id="forExport" >')
    printWindow.document.write(document.getElementById('forExport').innerHTML)

    if (!mapTitle) {
      printWindow.document.querySelector('.textInput').remove()
    }
    if (!legendText1) {
      printWindow.document.getElementById('legendText1').remove()
    }
    if (!legendText2) {
      printWindow.document.getElementById('legendText2').remove()
    }

    if (!legendText1 && !legendText2) {
      printWindow.document.getElementById('legend').remove()
    }

    printWindow.document.write('</div>')
    printWindow.document.write('</body></html>')
    printWindow.document.close()
    printWindow.onload = function () {
      // necessary if the div contain images
      printWindow.focus()
      printWindow.moveTo(0, 0)

      // const isChromium = window.chrome

      switch (type) {
        case 'png':
          // if (isChromium) {
          html2canvas(printWindow.document.getElementById('forExport') as HTMLElement, {
            backgroundColor: null,
            foreignObjectRendering: true,
            x: 0,
            y: 0,
            scale: 4,
          }).then((canvas) => {
            const downloadStageLink = window.document.createElement('a')
            downloadStageLink.href = canvas.toDataURL('image/png')
            downloadStageLink.download = 'Wie doet er mee kaart'
            downloadStageLink.click()
            printWindow.close()
          })
          // } else {
          //   toPng(printWindow.document.getElementById('forExport') as HTMLElement, {}).then((dataUrl) => {
          //     // TODO png firefox fixen
          //     const downloadStageLink = window.document.createElement('a')
          //     downloadStageLink.href = dataUrl
          //     downloadStageLink.download = 'Wie doet er mee kaart'
          //     downloadStageLink.click()
          //     printWindow.close()
          //   })
          // }
          break
        case 'jpeg':
          // if (isChromium) {
          html2canvas(printWindow.document.getElementById('forExport') as HTMLElement, {
            backgroundColor: 'white',
            foreignObjectRendering: true,
            scale: 4,
          }).then((canvas) => {
            const downloadStageLink = window.document.createElement('a')
            downloadStageLink.href = canvas.toDataURL('image/jpeg')
            downloadStageLink.download = 'Wie doet er mee kaart'
            downloadStageLink.click()
            printWindow.close()
          })
          // } else {
          //   // TODO jpeg firefox fixen
          //   toJpeg(printWindow.document.getElementById('forExport') as HTMLElement, {
          //     backgroundColor: 'white',
          //     canvasWidth: 800,
          //     canvasHeight: 800,
          //   }).then((dataUrl) => {
          //     const downloadStageLink = window.document.createElement('a')
          //     downloadStageLink.href = dataUrl
          //     downloadStageLink.download = 'Wie doet er mee kaart'
          //     downloadStageLink.click()
          //     printWindow.close()
          //   })
          // }
          break
        case 'pdf':
          setTimeout(function () {
            printWindow.print()
            printWindow.close()
          }, 250)
          break

        default:
          break
      }
    }
  }

  const downloadOptions = [
    { value: 'jpeg', label: 'Download als JPEG' },
    { value: 'png', label: 'Download als PNG' },
    { value: 'pdf', label: 'Download als PDF' },
  ]

  const handleDownload = (e) => {
    openPrintWindow(e.value)
    setDownloadValue(null)
  }

  const handleYearClick = (e) => {
    setYear(e.target.value)
  }

  return (
    <StyledMapContainer>
      <DownloadContainer id="forExport">
        <TitleInput
          value={mapTitle}
          onChange={(e) => setMapTitle(e.target.value)}
          placeholder="Vul een titel in.. "
          name="titleInput"
          id="title"
          className="textInput"
        />
        <Map year={year} />
        <Legend id="legend">
          <h3 style={{ margin: 0, marginBottom: '0.5rem' }}>Legenda</h3>

          <LegendItem id="legendText1" className="legendItem">
            <div
              style={{
                backgroundColor: '#d2d2d2',
              }}
            ></div>
            <TextInput
              className="textInput"
              value={legendText1}
              onChange={(e) => setLegendText1(e.target.value)}
              name="Wie doet er mee?"
              placeholder="Vul in .."
            />
          </LegendItem>

          <LegendItem id="legendText2" className="legendItem">
            <div
              style={{
                backgroundColor: '#0b71a1',
              }}
            ></div>
            <TextInput
              className="textInput"
              value={legendText2}
              onChange={(e) => setLegendText2(e.target.value)}
              name="Wie doet er mee?"
              placeholder="Vul in .."
            />
          </LegendItem>
        </Legend>
        <Source id="source">
          <p>Bron: CBS </p>
          <StyledSelectSource id="source-select" onChange={handleYearClick}>
            <option value="2023">2023</option>
            <option value="2022">2022</option>
            <option value="2021">2021</option>
          </StyledSelectSource>
          <VNGLogo />
        </Source>
      </DownloadContainer>
      <ExportContainer>
        <p>Klaar met bewerken?</p>
        <StyledSelectComponent
          id="downloadStageButton"
          name="option"
          isSearchable={false}
          placeholder={'Exporteer kaart'}
          options={downloadOptions}
          menuPlacement="top"
          value={downloadValue}
          onChange={(e) => handleDownload(e)}
        />
      </ExportContainer>
    </StyledMapContainer>
  )
}
export default MapContainer
