// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

// check if input contains the following names:

export const checkNotMunicipality = (element) => {
  const wrongValues = [
    'provincie',
    'brandweer',
    'kadaster',
    'archief',
    'bedrijfsvoeringsorganisatie',
    'omgevingsdienst',
    'rvo',
    '1stroom',
    'waterschap',
  ]
  const compareString = element.toLowerCase()
  compareString.replace(/gemeente\b/g, '')
  // console.log(`>>> ${compareString} `);
  wrongValues.forEach((element) => {
    const regex = new RegExp(String(element), 'g')
    // console.log(regex.test(compareString));

    if (regex.test(element)) {
      return {
        input: element,
        gm_code: '',
        gm_name: '',
        status: 'x',
        complete: true,
        onMap: false,
      }
    }
    return {
      input: element,
      gm_code: '',
      gm_name: '',
      status: '',
      complete: false,
      onMap: false,
    }
  })
}

export const verifyName = async (element) => {
  const compareString = element.input.toLowerCase().replace(/gemeente /g, '')
  try {
    const response = await fetch(
      `https://api.pdok.nl/bzk/locatieserver/search/v3_1/free?rows=1&fq=type:(gemeente)&q=${encodeURIComponent(
        compareString,
      )}`,
    )
    if (!response) {
      return null
    }

    const data = await response.json()

    if (data.response.docs?.[0]) {
      return {
        input: element.input,
        gm_code: `GM${data.response.docs[0].gemeentecode}`,
        gm_name: data.response.docs[0].gemeentenaam,
        status: compareString === data.response.docs[0].gemeentenaam.toLowerCase() ? 'v' : 'o',
        onMap: compareString === data.response.docs[0].gemeentenaam.toLowerCase() ? true : false,
        complete: true,
      }
    }

    return {
      input: element.input,
      gm_code: '',
      gm_name: '',
      status: 'x',
      onMap: false,
      complete: true,
    }
  } catch (err) {
    console.warn(err)
    throw err
  }
}

// drechtsteden
// buch
// bar organisatie
// sed
// cgm -> land vn cuijck

export const organisations = {
  bergen: [
    {
      input: '',
      gm_code: 'GM0373',
      gm_name: 'Bergen (NH)',
      status: 'o',
      complete: true,
      onMap: false,
    },
    {
      input: '',
      gm_code: 'GM0893',
      gm_name: 'Bergen (L)',
      status: 'o',
      complete: true,
      onMap: false,
    },
  ],
  drechtsteden: [
    {
      input: '',
      gm_code: '',
      gm_name: 'Dordrecht',
      status: 'o',
      complete: true,
      onMap: true,
    },
    {
      input: '',
      gm_code: '',
      gm_name: 'Zwijndrecht',
      status: 'o',
      complete: true,
      onMap: true,
    },
    {
      input: '',
      gm_code: '',
      gm_name: 'Hendrik-Ido-Ambacht',
      status: 'o',
      complete: true,
      onMap: true,
    },
    {
      input: '',
      gm_code: '',
      gm_name: 'Alblasserdam',
      status: 'o',
      complete: true,
      onMap: true,
    },
    {
      input: '',
      gm_code: '',
      gm_name: 'Papendrecht',
      status: 'o',
      complete: true,
      onMap: true,
    },
    {
      input: '',
      gm_code: '',
      gm_name: 'Sliedrecht',
      status: 'o',
      complete: true,
      onMap: true,
    },
    {
      input: '',
      gm_code: '',
      gm_name: 'Hardinxveld-Giessendam',
      status: 'o',
      complete: true,
      onMap: true,
    },
  ],
  buch: [
    {
      input: '',
      gm_code: 'GM0373',
      gm_name: 'Bergen',
      status: 'o',
      complete: true,
      onMap: true,
    },
    {
      input: '',
      gm_code: 'GM0450',
      gm_name: 'Uitgeest',
      status: 'o',
      complete: true,
      onMap: true,
    },
    {
      input: '',
      gm_code: 'GM0383',
      gm_name: 'Castricum',
      status: 'o',
      complete: true,
      onMap: true,
    },
    {
      input: '',
      gm_code: 'GM0399',
      gm_name: 'Heiloo',
      status: 'o',
      complete: true,
      onMap: true,
    },
  ],
  bar: [
    {
      input: '',
      gm_code: 'GM0489',
      gm_name: 'Barendrecht',
      status: 'o',
      complete: true,
      onMap: true,
    },
    {
      input: '',
      gm_code: 'GM0613',
      gm_name: 'Albrandswaard',
      status: 'o',
      complete: true,
      onMap: true,
    },
    {
      input: '',
      gm_code: 'GM0597',
      gm_name: 'Ridderkerk',
      status: 'o',
      complete: true,
      onMap: true,
    },
  ],
}
