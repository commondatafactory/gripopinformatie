// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import * as XLSX from 'xlsx'
import styled from 'styled-components'
import { Button } from '@commonground/design-system'

const StyledInput = styled.input`
  display: none;
`

const InputFile = ({ setArrayData, setFileName }: any) => {
  const openfile = () => {
    document.getElementById('file-upload').click()
  }
  const readFile = (filePath) => {
    // var f = filePath.state.file;
    const reader = new FileReader()
    reader.onload = (evt) => {
      // evt = on_file_select event
      /* Parse data */
      const bstr = evt.target.result
      const wb = XLSX.read(bstr, { type: 'binary' })
      /* Get first worksheet */
      const wsname = wb.SheetNames[0]
      const ws = wb.Sheets[`${wsname}`]
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_csv(ws)
      /* Update state */
      setArrayData(convertToArray(data))
    }
    reader.readAsArrayBuffer(filePath)
  }
  const convertToArray = (csv) => {
    const lines = csv.split('\n')
    const result = []
    const headers = lines[0].split(',')
    for (let i = 0; i < lines.length; i++) {
      const currentline = lines[`${i}`].split(',')
      for (let j = 0; j < headers.length; j++) {
        result.push(currentline[`${j}`])
      }
    }
    return result
  }

  return (
    <div>
      <Button variant="secondary" onClick={openfile}>
        Upload Excel document
      </Button>
      <StyledInput
        type="file"
        id="file-upload"
        onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
          const file: Blob = e.target.files[0]
          setFileName(e.target.files[0].name)
          readFile(file)
        }}
        accept=".xlsx, .xls"
      />
    </div>
  )
}

export default InputFile
