// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { useEffect, useState, useContext } from 'react'
import styled from 'styled-components'

import InputFile from '../InputFile/InputFile'
import FileIcon from '../../../../public/file.svg'
import CloseIcon from '../../../../public/close.svg'

import { organisations, verifyName } from '../helperFunctions'
import AppContext, { Item } from '../../../contextAPI'

const StyledUpload = styled.div`
  background-color: white;
  border-radius: 15px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 10px;
  padding-right: 2rem;

  p {
    margin: 0;
  }

  .grey {
    color: ${(p) => p.theme.tokens.colorPaletteGrey800};
    color: grey;
    font-size: 16px;
    font-weight: 400;
  }
`

const StyledCloseIcon = styled(CloseIcon)`
  cursor: pointer;
  height: 40px;
`

const StyledImageDiv = styled.div`
  background-color: #0b71a1;
  border-radius: 15px 0px 0px 15px;
  width: 90px;
  justify-self: stretch;
  height: 90px;
  display: flex;
  align-items: center;
  justify-content: center;
`

const UploadExcel = function ({ setMapDataTotal }: any) {
  const [arrayData, setArrayData] = useState<string[]>()
  const [fileName, setFileName] = useState('')
  const { fileLoaded, setFileLoaded, setMapData } = useContext(AppContext)

  useEffect(() => {
    if (arrayData?.length && fileName) {
      // unique array
      const uniqueArray = arrayData.filter((item, pos) => arrayData.indexOf(item) === pos)
      // remove empty
      uniqueArray.join('').split('')
      const dataset: Item[] = []

      uniqueArray.forEach((element) => {
        if (element && element.length > 1) {
          const compareString = element.toLowerCase().replace(/gemeenten/g, '')
          if (compareString.trim() in organisations) {
            const obj = organisations[compareString.trim()]
            obj.forEach((item) => {
              dataset.push({ ...item, input: element })
            })
          } else {
            dataset.push({
              input: element,
              gm_code: '',
              gm_name: '',
              status: 'x',
              complete: undefined,
            })
          }
        }
      })
      setMapDataTotal(dataset.length)
      setFileLoaded(true)

      const setUpdatedMapData = async (baseData = dataset, updatedData = [], sampleSize = 5) => {
        const partialData = baseData.slice(updatedData.length, updatedData.length + sampleSize)

        const data = await Promise.all(partialData.map(async (item) => (item.complete ? item : verifyName(item))))

        const newData = [...updatedData, ...data]

        setMapData(newData)
        if (newData.length < baseData.length) {
          setUpdatedMapData(baseData, newData)
        }
      }
      setUpdatedMapData()
    }
  }, [arrayData, fileName])

  const resetAll = () => {
    setFileName('')
    setArrayData([])
    setFileLoaded(false)
    setMapData([])
    setMapDataTotal(0)
  }

  return (
    <>
      {fileLoaded && <p>Gekozen Excel</p>}
      <StyledUpload className="uploadExcel">
        {fileLoaded && (
          <StyledImageDiv>
            <FileIcon alt="file is uploaded" />
          </StyledImageDiv>
        )}
        {!fileLoaded ? (
          <>
            <div style={{ margin: '2rem' }}>
              <p>Upload een Excel document om te beginnen</p>
              <p className="grey">Zorg dat er maar 1 kolom met alle gemeenten in het document staat</p>
            </div>
            <InputFile setArrayData={setArrayData} setFileName={setFileName} />
          </>
        ) : (
          <>
            <h4 style={{ marginRight: '1rem', marginLeft: '1rem' }}>{fileName}</h4>
            <StyledCloseIcon onClick={resetAll} alt="verwijder bestand" />
          </>
        )}
      </StyledUpload>
    </>
  )
}

export default UploadExcel
