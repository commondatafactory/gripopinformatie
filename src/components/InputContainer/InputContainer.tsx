// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React, { useContext, useEffect, useState } from 'react'
import styled, { css } from 'styled-components'
import { Button, Spinner } from '@commonground/design-system'
import AppContext from '../../contextAPI'
import CompleteItem from './CompleteItem/CompleteItem'
import UploadExcel from './UploadExcel/UploadExcel'

const StyledDiv = styled.div`
  height: 100vh;
  width: 50%;
  background-color: ${(div) => div.theme.tokens.colorPaletteGray100};
  padding: ${(div) => div.theme.tokens.spacing10};
  overflow: scroll;

  display: flex;
  flex-direction: column;

  @media only screen and (max-width: 1080px) {
    & {
      width: 100%;
      height: auto;
    }
  }

  .grey {
    color: ${(p) => p.theme.tokens.colorPaletteGrey800};
    color: grey;
    font-size: 16px;
    font-weight: 400;
    margin-top: 1rem;
  }
  #contact {
    text-align: center;
    margin-top: auto;
  }
`

const Tabs = styled.div`
  height: fit-content;
  width: 100%;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  column-gap: 0.25rem;
  position: relative;
`
const StyledButton = styled(Button)`
  text-align: left;
  height: 100%;

  ${({ isActive }) =>
    isActive &&
    css`
      color: white;
      background-color: #0b71a1;
    `}

  :hover,
  :focus {
    ${({ isActive }) =>
      isActive &&
      css`
        color: white;
        background-color: #0b71a1;
      `}
  }
`

const StyledTable = styled.table`
  margin: 25px 0;
  border-collapse: collapse;
  empty-cells: hide;
  width: 100%;

  thead tr {
    text-align: left;
  }
  th {
    font-weight: normal;
    padding: 9px 15px;
  }
  td {
    padding: 9px 15px;
    vertical-align: bottom;
  }
  tr {
    border-bottom: 1px solid #e0e0e0;
  }
`

const OptionsContainer = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 2rem;
  height: 2rem;
  border-radius: 1rem;
  cursor: pointer;
  margin-right: 16px;
  flex-shrink: 0;
  color: white;
`

const UndeterminedTag = styled(OptionsContainer)`
  background-color: #f02b41;
`
const ActiveTag = styled(OptionsContainer)`
  background-color: #39870c;
`
const ReviseTag = styled(OptionsContainer)`
  background-color: #ffbc2c;
`

const Loader = styled('div')`
  display: flex;
`

const InputContainer = function () {
  const [totalWrong, setTotalWrong] = useState(0)
  const [totalCorrect, setTotalCorrect] = useState(0)
  const [totalCheck, setTotalCheck] = useState(0)
  const [statusView, setStatusView] = useState('o')
  const { mapData, setUpdateItem, setHoverFeature, fileLoaded } = useContext(AppContext)
  const [mapDataTotal, setMapDataTotal] = useState(0)

  useEffect(() => {
    if (mapData) {
      setTotalWrong(mapData.reduce((n, obj) => n + (obj.status === 'x' ? 1 : 0), 0))
      setTotalCheck(mapData.reduce((n, obj) => n + (obj.status === 'o' ? 1 : 0), 0))
      setTotalCorrect(mapData.reduce((n, obj) => n + (obj.status === 'v' ? 1 : 0), 0))
    }
  }, [mapData])

  const handleRemove = (item) => {
    item.onMap = false

    let count = 0
    for (let i = 0; i < mapData.length; i++) {
      if (mapData[Number(i)].gm_code === item.gm_code && mapData[Number(i)].onMap) {
        count++
      }
    }
    if (count >= 1) {
      // do not remove from map
    }
    if (count === 0) {
      setUpdateItem({
        gm_code: item.gm_code,
        add: false,
      })
    }
  }

  const handleAdd = (item) => {
    item.onMap = true
    setUpdateItem({
      gm_code: item.gm_code,
      add: true,
    })
  }

  return (
    <StyledDiv>
      <h1>Maak gemeentekaart </h1>
      <UploadExcel setMapDataTotal={setMapDataTotal} />
      <Loader>
        {mapDataTotal && mapData.length < mapDataTotal ? <Spinner /> : null}
        {fileLoaded && (
          <p>
            {mapData.length} items van {mapDataTotal} items opgehaald
          </p>
        )}
      </Loader>
      {mapData?.length > 1 && (
        <>
          <Tabs>
            <StyledButton isActive={statusView === 'o'} onClick={() => setStatusView('o')} variant="secondary">
              <ReviseTag>{totalCheck}</ReviseTag>Mogelijke gemeenten
            </StyledButton>
            <StyledButton isActive={statusView === 'x'} onClick={() => setStatusView('x')} variant="secondary">
              <UndeterminedTag>{totalWrong}</UndeterminedTag>Onbekend
            </StyledButton>
            <StyledButton isActive={statusView === 'v'} onClick={() => setStatusView('v')} variant="secondary">
              <ActiveTag>{totalCorrect}</ActiveTag> Gemeenten
            </StyledButton>
          </Tabs>

          {statusView === 'x' && (
            <p className="grey">
              De volgende gemeenten kunnen niet gevonden worden. Pas de namen aan in uw Excel bestand en probeer het
              opnieuw.
            </p>
          )}
          {statusView === 'o' && <p className="grey">Selecteer gemeenten om ze op de kaart te zien</p>}
          <StyledTable>
            <thead>
              <tr>
                <th />
                <th>NAAM UIT EXCEL</th>
                <th>GEVONDEN GEMEENTE</th>
                <th>GEMEENTE CODE</th>
              </tr>
            </thead>
            <tbody>
              {mapData
                .filter((obj) => obj.status === statusView)
                .map((item) =>
                  item ? (
                    <CompleteItem
                      key={item.input + item.gm_name}
                      item={item}
                      handleRemove={handleRemove}
                      handleAdd={handleAdd}
                      setHoverFeature={setHoverFeature}
                    />
                  ) : null,
                )}
            </tbody>
          </StyledTable>
        </>
      )}
      <p id="contact">
        <a href="mailto:vip@vng.nl">contact: vip@vng.nl</a>
      </p>
    </StyledDiv>
  )
}

export default InputContainer
